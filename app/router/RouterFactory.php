<?php

namespace App;

use Nette;
use Nette\Application\Routers\RouteList;


final class RouterFactory
{
	use Nette\StaticClass;

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList;

        $router[] = SecurityModule\Routes::get();
		$router[] = SiteModule\Routes::get();
		$router[] = UserModule\Routes::get();
		$router[] = TheaterModule\Routes::get();
		$router[] = PerformanceModule\Routes::get();
		$router[] = EventModule\Routes::get();
		$router[] = TicketModule\Routes::get();

		return $router;
	}
}
