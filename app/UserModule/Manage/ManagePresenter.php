<?php

namespace App\UserModule;


use App\Model\Entities\User;
use App\Model\Entities\UserRole;
use App\Forms\FormFactory;
use App\Model\Facades\UserFacade;
use App\Presenters\BasePresenter;
use Nette\Application\UI\Form;


class ManagePresenter extends BasePresenter
{
    /** @var int @persistent */
    public $id;

    /** @var FormFactory */
    private $formFactory;

    /** @var User */
    private $user;

    /** @var UserFacade */
    private $users;

    public function __construct(FormFactory $formFactory, UserFacade $users)
    {
        parent::__construct();
        $this->formFactory = $formFactory;
        $this->users = $users;
    }

    public function renderDefault($id): void
    {
        if($this->id){
            /** @var User user */
            $this->user = $this->users->getUser($id);
        }
    }

    public function save(Form $form, $values): void
    {
        $this->user = new User();
        if($this->id){

        } else {

        }
    }

    protected function createComponentManageUser(): Form
    {
        $usersRoleList = $this->users->getUsersRole();
        $usersRole = [null => null];
        /** @var UserRole $ur */
        foreach($usersRoleList as $ur){
            $usersRole[$ur->id] = 'ui.userRole.' . $ur->name;
        }

        $form = $this->formFactory->create();
        $form->setTranslator($this->translator);
        $form->addHidden('id');
        $form->addText('name', 'ui.user.name')
            ->setDefaultValue($this->user->name ?? null)
            ->setRequired(true);
        $form->addText('surname', 'ui.user.surname')
            ->setDefaultValue($this->user->surname ?? null)
            ->setRequired(true);
        $form->addText('email', 'ui.user.email')
            ->setDefaultValue($this->user->email ?? null)
            ->setRequired(true);
        $form->addSelect('user_role_id', 'ui.user.role', $usersRole)
            ->setDefaultValue($this->user->UserRole->id ?? null)
            ->setRequired(true);
        $form->addText('color', 'ui.user.color')
            ->setDefaultValue($this->user->color ?? null)
            ->setType('color')
            ->setHtmlAttribute('class', 'color')
            ->setRequired(true);
        $form->addCheckbox('is_blocked', 'ui.user.isBlocked')
            ->setDefaultValue($this->user->isBlocked ?? 0);

        $form->addSubmit('save', 'ui.user.save')
            ->setHtmlAttribute('class', 'pull-right');
        $form->addSubmit('back', 'ui.navigation.back')
            ->setHtmlAttribute('class', 'btn-default pull-right mr-1')
            ->onClick[] = [$this,'goBack'];
        $form->onSuccess[] = [$this, 'save'];

        return $form;
    }

    public function goBack()
    {
        $this->redirect('Default:');
    }
}