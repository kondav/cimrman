<?php

namespace App\UserModule;


use App\Model\Entities\User;
use App\Model\Facades\UserFacade;
use App\Presenters\BasePresenter;


class DefaultPresenter extends BasePresenter
{
    /** @var UserFacade */
    private $users;

    public function __construct(UserFacade $users)
    {
        parent::__construct();
        $this->users = $users;
    }

    public function renderDefault(): void
    {
        $this->template->users = $this->users->getUsers();
    }
}