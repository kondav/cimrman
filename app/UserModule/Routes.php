<?php

namespace App\UserModule;

use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;

class Routes
{
    public static function get()
    {
        $list = new RouteList('User');

        $list[] = new Route('/users', 'Default:default');
        $list[] = new Route('/users/manage-user', 'Manage:default');
        $list[] = new Route('/users/new-users', 'Default:newUsers');

        return $list;
    }
}