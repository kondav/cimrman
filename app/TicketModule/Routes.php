<?php

namespace App\TicketModule;

use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;


class Routes
{
    public static function get()
    {
        $list = new RouteList('Ticket');

        $list[] = new Route('/tickets', 'Default:default');

        return $list;
    }
}