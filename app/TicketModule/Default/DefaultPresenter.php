<?php

namespace App\TicketModule;

use App\Forms\FormFactory;
use App\Model\Facades\TheaterFacade;
use App\Presenters\BasePresenter;
use Nette\Application\UI\Form;


class DefaultPresenter extends BasePresenter
{
    /** @var FormFactory */
    private $formFactory;

    /** @var TheaterFacade */
    private $theaterFacade;

    public function __construct(FormFactory $formFactory, TheaterFacade $theaterFacade)
    {
        parent::__construct();
        $this->formFactory = $formFactory;
        $this->theaterFacade = $theaterFacade;
    }

    public function createComponentPageForm(): Form
    {
        $form = $this->formFactory->create();
        $form->setTranslator($this->translator);

        $theaterList = $this->theaterFacade->getAll();

        $form->addSelect('theater', 'ui.selectPage.theater')
            ->setRequired(TRUE);
        $form->addSelect('event', 'ui.selectPage.event')
            ->setRequired(TRUE);
        $form->addSelect('performance', 'ui.selectPage.performance')
            ->setRequired(TRUE);
        $form->addSubmit('open', 'ui.selectPage.openPerformance');
        $form->onSuccess[] = [$this, 'getPage'];

        return $form;
    }
}