<?php

namespace App\SiteModule;


use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;


class Routes
{
    public static function get()
    {
        $list = new RouteList('Site');

        $list[] = new Route('/', 'Homepage:default');

        return $list;
    }
}