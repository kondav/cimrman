<?php

namespace App\PerformanceModule;

use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;

class Routes
{
    public static function get()
    {
        $list = new RouteList('Performance');

        $list[] = new Route('/performances', 'Default:default');

        return $list;
    }
}