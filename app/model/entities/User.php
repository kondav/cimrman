<?php

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\MagicAccessors;


/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User extends BaseEntity
{
    use MagicAccessors;

    const ROLE_ADMIN = 1;
    const ROLE_MANAGER = 2;
    const ROLE_USER = 3;

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $surname;


    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $email;

    /**
     * @var UserRole
     * @ORM\ManyToOne(targetEntity="UserRole",inversedBy="idUserRole",cascade={"persist"})
     * @ORM\JoinColumn(name="user_role_id",referencedColumnName="id", nullable=true)
     */
    protected $UserRole;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $color;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $password;

    /**
     * @var int
     * @ORM\Column(name="is_blocked", type="smallint")
     */
    protected $isBlocked = 0;

    /**
     * @var int
     * @ORM\Column(name="is_deleted", type="smallint")
     */
    protected $isDeleted = 0;

    /**
     * @var int
     * @ORM\Column(name="is_new", type="smallint")
     */
    protected $isNew = 1;

    public function isAdmin()
    {
        return isset($this->UserRole) ? $this->UserRole->id === self::ROLE_ADMIN : NULL;
    }
}