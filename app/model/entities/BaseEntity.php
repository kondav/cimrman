<?php

namespace App\Model\Entities;


class BaseEntity
{
    public function setPropertiesFromArray(array $data, bool $onlyExists = true): void
    {
        foreach ($data as $property => $val) {
            if($onlyExists){
                if(property_exists($this, $property)){
                    $this->$property = $data[$property];
                }
            } else {
                $this->$property = $data[$property];
            }
        }
    }
}