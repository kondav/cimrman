<?php

namespace App\Model\Facades;


use Kdyby\Doctrine\EntityManager;


abstract class BaseFacade
{
    /**@var EntityManager */
    protected $em;

    public function __construct(EntityManager $em){
        $this->em = $em;
    }
}