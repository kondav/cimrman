<?php

namespace App\Model\Facades;


use App\Model\Entities\Theater;


class TheaterFacade extends BaseFacade
{
    public function getAll(): array
    {
        return $this->em->getRepository(Theater::class)->findAll();
    }

    public function getOne(int $id): ?Theater
    {
        return $this->em->find(Theater::class, $id);
    }

    public function create(array $values): Theater
    {
        /** @var Theater $newTheater */
        $newTheater = new Theater();
        $newTheater->setPropertiesFromArray($values);
        $this->em->persist($newTheater);
        $this->em->flush();
        $this->em->refresh($newTheater);
        return $newTheater;
    }

    public function update(array $values): Theater
    {
        /** @var Theater $theater */
        $theater = $this->em->find(Theater::class, $values['id']);
        $theater->setPropertiesFromArray($values);
        $this->em->flush();
        $this->em->refresh($theater);
        return $theater;
    }
}