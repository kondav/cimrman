<?php

namespace App\Model\Facades;


use App\Model\Entities\User;
use App\Model\Entities\UserRole;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Nette\Security\Passwords;
use Nette\Utils\ArrayHash;


class UserFacade extends BaseFacade implements IAuthenticator
{
    public function getUser(int $id): ?User
    {
        return $this->em->find(User::class, $id);
    }

    public function getUsers(): array
    {
        return $this->em->getRepository(User::class)->findAll();
    }

    public function getUsersRole(): array
    {
        return $this->em->getRepository(UserRole::class)->findAll();
    }

    public function singUp(ArrayHash $values): void
    {
        /** @var User $newUser */
        $newUser = new User();
        $newUser->email = $values->email;
        $newUser->name = $values->name;
        $newUser->surname = $values->surname;
        $newUser->password = password_hash($values->password, PASSWORD_BCRYPT, ['cost' => 12]);
        $newUser->isNew = 1;
        $this->em->persist($newUser);
        $this->em->flush();
    }

    public function authenticate(array $credentials)
    {
        list($email, $password) = $credentials;

        /** @var User $user */
        $user = $this->em->getRepository(User::class)->findOneBy(['email' => $email]);

        if(!isset($user) || !Passwords::verify($password, $user->password)) throw new AuthenticationException('youFilledBadNameOrPassword');

        if($user->isNew) throw new AuthenticationException('yourAccountIsNotApproved');

        if(Passwords::needsRehash($user->password)){
            $user->password = password_hash($password, PASSWORD_BCRYPT, ['cost' => 12]);
            $this->em->flush();
        }

        return new Identity($user->id);
    }

    public function getCountOfNewUsers()
    {
        return $this->em->getRepository(User::class)->countBy(['isNew' => 1]);
    }
}