<?php

namespace App\TheaterModule;


use App\Model\Facades\TheaterFacade;
use App\Presenters\BasePresenter;


class DefaultPresenter extends BasePresenter
{
    /** @var TheaterFacade */
    private $theaters;

    public function __construct(TheaterFacade $theaters)
    {
        parent::__construct();
        $this->theaters = $theaters;
    }

    public function renderDefault(): void
    {
        $this->template->theaters = $this->theaters->getAll();
    }
}