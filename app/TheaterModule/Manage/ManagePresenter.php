<?php

namespace App\TheaterModule;


use App\Model\Entities\Theater;
use App\Forms\FormFactory;
use App\Model\Facades\TheaterFacade;
use App\Presenters\BasePresenter;
use Nette\Application\UI\Form;


class ManagePresenter extends BasePresenter
{
    /** @var int @persistent */
    public $id;

    /** @var FormFactory */
    private $formFactory;

    /** @var Theater */
    private $theater;

    /** @var TheaterFacade */
    private $theaters;

    public function __construct(FormFactory $formFactory, TheaterFacade $theaters)
    {
        parent::__construct();
        $this->formFactory = $formFactory;
        $this->theaters = $theaters;
    }

    public function renderDefault($id): void
    {
        if($this->id){
            /** @var Theater user */
            $this->theater = $this->theaters->getOne($id);
        }
    }

    public function save(Form $form, array $values): void
    {
        try{
            if($this->id){
                $theater = $this->theaters->update($values);
            } else {
                $theater = $this->theaters->create($values);
            }
        } catch(\Exception $exception){
            $this->flashMessage($this->translator->translate('ui.theater.thisNameIsExists'), 'error');
            return;
        }
        $this->flashMessage($this->translator->translate('ui.theater.saved'), 'success');
        $this->redirect('Details:', $theater->id);
    }

    protected function createComponentManageTheater(): Form
    {
        $form = $this->formFactory->create();
        $form->setTranslator($this->translator);
        $form->addHidden('id')
            ->setDefaultValue($this->theater->id ?? null);
        $form->addText('name', 'ui.theater.name')
            ->setDefaultValue($this->theater->name ?? null)
            ->setRequired(TRUE);

        $form->addSubmit('save', 'ui.theater.save')
            ->setHtmlAttribute('class', 'pull-right');
        $form->addSubmit('back', 'ui.navigation.back')
            ->setHtmlAttribute('class', 'btn-default pull-right mr-1')
            ->onClick[] = [$this,'goBack'];
        $form->onSuccess[] = [$this, 'save'];

        return $form;
    }

    public function goBack()
    {
        $this->redirect('Default:');
    }
}