<?php

namespace App\TheaterModule;

use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;

class Routes
{
    public static function get()
    {
        $list = new RouteList('Theater');

        $list[] = new Route('/theaters', 'Default:default');
        $list[] = new Route('/theaters/manage', 'Manage:default');
        $list[] = new Route('/theaters/detail/<id>', 'Details:default');

        return $list;
    }
}