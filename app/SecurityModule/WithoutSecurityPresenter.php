<?php

namespace App\SecurityModule;


use Kdyby\Translation\Translator;
use Nette\Application\UI\Presenter;


class WithoutSecurityPresenter extends Presenter
{
    /** @var Translator @inject */
    public $translator;

    public function beforeRender()
    {
        $this->template->setTranslator($this->translator);
    }

    public function formatTemplateFiles(): array
    {
        $list = [];

        $list[] = \dirname($this->getReflection()->getFilename()) . '/' . $this->getView() . '.latte';
        $list[] = \dirname($this->getReflection()->getFilename()) . '/' .
            explode(':', $this->getPresenter()->name)[1] . '/' . $this->getView() . '.latte';
        $list[] = \dirname($this->getReflection()->getFilename()) . '/templates/' . $this->getView() . '.latte';

        return $list;
    }
}