<?php

namespace App\SecurityModule;


use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;


class Routes
{
    public static function get()
    {
        $list = new RouteList('Security');

        $list[] = new Route('/registration', 'Sign:default');
        $list[] = new Route('/login', 'Login:default');

        return $list;
    }
}