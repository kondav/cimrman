<?php

namespace App\SecurityModule;

use App\Forms\SignFormFactory;
use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;


class LoginPresenter extends WithoutSecurityPresenter
{
    /**@var SignFormFactory */
    private $formFactory;

    public function __construct(SignFormFactory $formFactory)
    {
        parent::__construct();
        $this->formFactory = $formFactory;
    }

    public function createComponentLoginForm()
    {
        $form = $this->formFactory->createSignInForm();
        $form->onSuccess[] = function(Form $form){
            $p = $form->getPresenter();
            $p->redirect(':Site:Homepage:');
        };
        return $form;
    }

    public function onSuccess(Form $form, $values): void
    {
        try{
            $this->login($values['username'], $values['password']);
        } catch (AuthenticationException $exception){
            Logger::error($exception->getMessage());
            $this->flashMessage($this->translator->translate($exception->getMessage()), 'error');
        }
    }

    private function login(string $username, $password = null): void
    {
        $this->user->login($username, $password);
        $this->redirect(':Site:Homepage:');
    }
}