<?php

namespace App\SecurityModule;


use App\Forms\SignFormFactory;
use Nette\Application\UI\Form;


class SignPresenter extends WithoutSecurityPresenter
{
    /** @var SignFormFactory */
    private $signFormFactory;

    public function __construct(SignFormFactory $signFormFactory)
    {
        parent::__construct();
        $this->signFormFactory = $signFormFactory;
    }
    protected function createComponentSingUpForm(): Form
    {
        $form = $this->signFormFactory->createSignUp();
        $form->onSuccess[] = function(Form $form){
            $p = $form->getPresenter();
            $p->flashMessage($this->translator->translate('ui.signUp.requestWasSent'), 'success');
            $p->redirect(':Security:Login:');
        };
        return $form;
    }
}