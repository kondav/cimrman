<?php

namespace App\Presenters;

use App\Model\Entities\User as UserEntity;
use App\Model\Facades\UserFacade;
use Kdyby\Translation\Translator;
use Nette;
use UserAuthenticationException;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    /** @var Translator @inject */
    public $translator;

    /** @var UserFacade
     * @inject
     */
    public $userFacade;

    /** @var UserEntity */
    public $userEntity;

    public function startup(): void
    {
        parent::startup();
            if($this->getUser()->isLoggedIn()){
                $this->userEntity = $this->userFacade->getUser($this->getUser()->getId());
            } else {
                $this->userEntity = new UserEntity();
                $this->redirect(':Security:Login:');
            }
    }

    public function beforeRender(): void
    {
        parent::beforeRender();
        $this->template->userEntity = $this->userEntity;
        if($this->userEntity->isAdmin()){
            $this->template->newUsers = $this->userFacade->getCountOfNewUsers();
        }
    }

    public function formatTemplateFiles(): array
    {
        $list = [];

        $list[] = \dirname($this->getReflection()->getFilename()) . '/' . $this->getView() . '.latte';
        $list[] = \dirname($this->getReflection()->getFilename()) . '/' .
            explode(':', $this->getPresenter()->name)[1] . '/' . $this->getView() . '.latte';
        $list[] = \dirname($this->getReflection()->getFilename()) . '/templates/' . $this->getView() . '.latte';

        return $list;
    }

    public function handleLogout(): void
    {
        $this->getUser()->logout(TRUE);
        $this->flashMessage($this->translator->translate('ui.logout.youWereLogout'), 'error');
        $this->redirect(':Security:Login:');
    }
}
