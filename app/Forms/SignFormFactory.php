<?php

namespace App\Forms;

use App\Model\Facades\UserFacade;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Kdyby\Translation\Translator;
use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;
use Nette\Security\User;
use Nette\Utils\ArrayHash;


class SignFormFactory extends FormFactory
{
    public function createSignUp(): Form
    {
        $form = $this->create();
        $form->setTranslator($this->translator);
        $form->addEmail('email', 'ui.signUp.email')
            ->setRequired(true);
        $form->addText('name', 'ui.signUp.name')
            ->setRequired(true);
        $form->addText('surname', 'ui.signUp.surname')
            ->setRequired(true);
        $form->addPassword('password', 'ui.signUp.password')
            ->setRequired(TRUE)
            ->addCondition(Form::FILLED)
            ->addRule(Form::MIN_LENGTH, 'ui.signUp.newPassTooShort', 5);
        $form->addPassword('password_again', 'ui.signUp.passwordAgain')
            ->setRequired(TRUE)
            ->addRule(Form::EQUAL, 'ui.signUp.notSamePasswords', $form['password']);

        $form->addSubmit('signUp', 'ui.signUp.sendRequest');

        $form->onSuccess[] = [$this, 'signUp'];

        return $form;
    }

    public function signUp(Form $form, ArrayHash $values): void
    {
        try {
            $this->userFacade->singUp($values);
        } catch (AuthenticationException $e){
            $form->addError($e->getMessage());
        } catch(UniqueConstraintViolationException $e){
            $form->addError('ui.signUp.userWithThisEmailAlreadyExists');
        } catch(\Exception $e){
            $form->addError($e->getMessage());
        }
    }

    public function createSignInForm(): Form
    {
        $form = $this->create();
        $form->setTranslator($this->translator);

        $form->addText('email', 'ui.signIn.email')
            ->setRequired(true);
        $form->addPassword('password', 'ui.signIn.password')
            ->setRequired(true);
        $form->addCheckbox('remember', 'ui.signIn.remember');
        $form->addSubmit('signIn', 'ui.signIn.logIn');

        $form->onSuccess[] = [$this, 'signIn'];

        return $form;
    }

    public function signIn(Form $form, ArrayHash $values): void
    {
        try {
            $this->user->login($values->email, $values->password);
            if ($values->remember) {
                $this->user->setExpiration("14 days", FALSE);
            } else {
                $this->user->setExpiration("20 minutes", TRUE);
            }
        } catch (AuthenticationException $e) {
            $form->addError($this->translator->translate("ui.signIn.{$e->getMessage()}"));
        }
    }
}