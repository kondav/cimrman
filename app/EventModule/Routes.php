<?php

namespace App\EventModule;

use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;

class Routes
{
    public static function get()
    {
        $list = new RouteList('Event');

        $list[] = new Route('/events', 'Default:default');

        return $list;
    }
}